function fadd() {
    var first, sec, res;
    first = parseFloat(document.forms[0].txt1st.value);
    sec = parseFloat(document.forms[0].txt2nd.value);
    res = first + sec;
    document.forms[0].txtresult.value = res;
}

function fminus() {
    var first, sec, res;
    first = parseFloat(document.forms[0].txt1st.value);
    sec = parseFloat(document.forms[0].txt2nd.value);
    res = first - sec;
    document.forms[0].txtresult.value = res;
}

function fdiv() {
    var first, sec, res;
    first = parseFloat(document.forms[0].txt1st.value);
    sec = parseFloat(document.forms[0].txt2nd.value);
    res = first / sec;
    document.forms[0].txtresult.value = res;
}

function fmult() {
    var first, sec, res;
    first = parseFloat(document.forms[0].txt1st.value);
    sec = parseFloat(document.forms[0].txt2nd.value);
    res = first * sec;
    document.forms[0].txtresult.value = res;
}