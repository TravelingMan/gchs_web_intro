Questions!

HTML
    inputs - select, checkbox, textbox, textarea, radio (password,file,button)
    label
    HR - horizontal rule
    BR vs P
    UL vs OL
    TH
    span vs div

CSS
    selectors
    padding vs margin
    border shorthand
    border width shorthand
    px vs em vs %
    common font props
    specificity

JS
    alert/confirm/prompt
    loop types - for vs while
    variable declaration / scope
    this keyword
    Array/Object declaration
    Array vs Object (behavior, looping)
    closure
    event bubbling 
    ternary operators

jQuery
    selectors - tags, classes, id, attributes, pseudo
    chaining
    traversing
    filter vs find
    append vs appendTo
    attr vs prop
    hide/show/toggle
    css
    each
    events
    is
    empty vs remove