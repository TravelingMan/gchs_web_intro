function updateTime(){
    
    // Get the current time
    var now = new Date();
    var hours = now.getHours();
    var minutes = now.getMinutes();
    var seconds = now.getSeconds();
    
    var timeOfDay = '?';
    
    // Make all the units two digits by adding a zero in front if needed.    
    if(minutes < 10){
        minutes = '0' + minutes;
    }
    
    if(seconds < 10){
        seconds = '0' + seconds;
    }
    
    // Is it AM or PM?
    if(hours >= 12 && hours<= 24){
        timeOfDay = 'pm';
    } else {
        timeOfDay = 'am';
    }
    
    // Splice these items together to create a string
    var currentTime = hours + ':' + minutes + ':' + seconds + timeOfDay;
    
    // Get the clock div
    var myClock = document.getElementById('clock');
    
    // Write the currentTime string to the clock div
    myClock.innerHTML = currentTime;
}