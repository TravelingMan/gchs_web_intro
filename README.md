GCHS: Introduction to Web Dev
=============================

_This is a work in progress. Contributions are welcome._

The purpose of this course is to introduce high school students to HTML
and CSS. The students do not have previous experience with any web
technologies.

Estimated time available for the course: 3 weeks.

Contents
--------
- Samples and code snippets
- Course planning
- Documentation
- Links to relevant tools and futher information
